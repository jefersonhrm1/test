from django.db import models
import pytz


class Client(models.Model):
    id = models.AutoField(primary_key=True)
    phone = models.BigIntegerField(unique=True)
    code_oper = models.IntegerField()
    tag = models.CharField(max_length=255)
    zones = tuple(zip(pytz.all_timezones, pytz.all_timezones))
    timezone = models.CharField(max_length=32, choices=zones, default='UTC')


class Sends(models.Model):
    id = models.AutoField(primary_key=True)
    date_start = models.DateTimeField()
    text = models.TextField()
    tags = models.TextField()
    date_end = models.DateTimeField()


class Message(models.Model):
    id = models.AutoField(primary_key=True)
    date_st = models.DateTimeField()
    status = models.CharField(max_length=255)
    id_sends = models.ForeignKey("Sends", on_delete=models.CASCADE)
    id_client = models.ForeignKey("Client", on_delete=models.CASCADE)

