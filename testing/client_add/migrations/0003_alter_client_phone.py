# Generated by Django 4.0.4 on 2022-04-14 09:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client_add', '0002_alter_client_phone'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='phone',
            field=models.BigIntegerField(unique=True),
        ),
    ]
