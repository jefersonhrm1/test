import datetime
import json
import re
import pytz
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from drf_yasg.utils import swagger_auto_schema
from .models import Client, Sends, Message
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from apscheduler.schedulers.background import BackgroundScheduler
import requests
from drf_yasg import openapi


pattern = re.compile('^(7)[\d]{10}$')


@swagger_auto_schema(method='post', request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'phone': openapi.Schema(type=openapi.TYPE_INTEGER, description='integer'),
        'tag': openapi.Schema(type=openapi.TYPE_STRING, description='string'),
        'timezone': openapi.Schema(type=openapi.TYPE_STRING, description='Africa/Abidjan'),
    }
))
@api_view(['POST'])
@csrf_exempt
def add_client(request):
    if request.method == 'POST':
        body = json.loads(request.body)
        try:
            if len(str(body["phone"])) != 11:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            phone = re.match(pattern, str(body["phone"]))
            if phone:
                client = Client()
                client.phone = body["phone"]
                client.code_oper = int(str(str(body["phone"])[1] + str(body["phone"])[2] + str(body["phone"])[3]))
                client.tag = body["tag"]
                client.timezone = body["timezone"]
                client.save()
        except Exception as err:
            return Response(err, status=status.HTTP_400_BAD_REQUEST)
    return Response(status=status.HTTP_201_CREATED)


@swagger_auto_schema(method='post', request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'phone': openapi.Schema(type=openapi.TYPE_INTEGER, description='integer'),
        'phone_new': openapi.Schema(type=openapi.TYPE_INTEGER, description='not required, will change phone'),
        'tag': openapi.Schema(type=openapi.TYPE_STRING, description='not required, will change tag'),
        'timezone': openapi.Schema(type=openapi.TYPE_STRING, description='not required, will change timezone'),
    }
))
@csrf_exempt
@api_view(['POST'])
def update_client(self, request):
    if request.method == 'POST':
        body = json.loads(request.body)
        try:
            update_keys = body.keys()
            if len(str(body["phone"])) != 11:
                return HttpResponse(status=400)
            phone = re.match(pattern, str(body["phone"]))
            client = Client.objects.filter(phone=body['phone']).first()
            if 'phone_new' in update_keys:
                if len(str(body["phone_new"])) != 11:
                    return HttpResponse(status=400)
                phone = re.match(pattern, str(body["phone_new"]))
                if phone:
                    client.phone = body["phone_new"]
                    client.code_oper = int(str(str(body["phone_new"])[1] + str(body["phone_new"])[2] +
                                           str(body["phone_new"])[3]))
            elif "tag" in update_keys:
                client.tag = body["tag"]
            elif "timezone" in update_keys:
                client.tag = body["tag"]
            client.save()
        except Exception as err:
            return HttpResponse(err, status=400)
    return HttpResponse(status=201)


@swagger_auto_schema(method='post', request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'phone': openapi.Schema(type=openapi.TYPE_INTEGER, description='integer'),
    }
))
@api_view(['POST'])
@csrf_exempt
def delete_client(self, request):
    if request.method == 'POST':
        body = json.loads(request.body)
        try:
            if len(str(body["phone"])) != 11:
                return HttpResponse(status=400)
            phone = re.match(pattern, str(body["phone"]))
            if phone:
                client = Client.objects.filter(phone=body['phone']).delete()
        except Exception as err:
            return HttpResponse(err, status=400)
    return HttpResponse(status=201)


@swagger_auto_schema(method='post', request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'date_start': openapi.Schema(type=openapi.TYPE_STRING, description='date "YYYY-MM-DD HH:MM"'),
        'text': openapi.Schema(type=openapi.TYPE_INTEGER, description='text of the message'),
        'tags': openapi.Schema(type=openapi.TYPE_STRING, description='tag or code of operator'),
        'date_end': openapi.Schema(type=openapi.TYPE_STRING, description='date "YYYY-MM-DD HH:MM"'),
    }
))
@api_view(['POST'])
@csrf_exempt
def add_send(self, request):
    if request.method == 'POST':
        body = json.loads(request.body)
        try:
            date_start = pytz.utc.localize(datetime.datetime.strptime(body["date_start"], '%Y-%m-%d %H:%M'))
            date_end = pytz.utc.localize(datetime.datetime.strptime(body["date_end"], '%Y-%m-%d %H:%M'))
            send = Sends()
            send.date_start = date_start
            send.date_end = date_end
            send.text = body["text"]
            send.tags = body["tags"]
            send.save()
            if pytz.utc.localize(datetime.datetime.utcnow()) >= date_start:
                scheduler.start_sends(send.tags, send.text, send, date_start, date_end)
            else:
                secs = (date_start - pytz.utc.localize(datetime.datetime.utcnow())).total_seconds()
                scheduler.start_sends(send.tags, send.text, send, date_start, date_end)
        except Exception as err:
            return HttpResponse(err, status=400)
    return HttpResponse(status=201)


class Scheduler:
    def __init__(self):
        self.scheduler = BackgroundScheduler()
        self.schedule = []

    def start_sends(self, tags, text, id_send, date_start, date_end):
        regex = re.match(r'^[\d]{3}$', tags)
        if regex:
            clients = Client.objects.filter(code_oper=tags)
        else:
            clients = Client.objects.filter(tag=tags)
        phones_ids = [(i, i.phone, id_send) for i in clients]
        self.scheduler.add_job(id=str(id_send.id), func=self.sends, args=(phones_ids, text), trigger='cron',
                               start_date=date_start, end_date=date_end, hour=12, minute=0)
        self.scheduler.start()

    def sends(self, phones, text):
        if len(self.schedule) > 0:
            self.retry()
        message = Message()
        for phone in phones:
            error = ''
            message.date_st = pytz.utc.localize(datetime.datetime.utcnow())
            message.status = "Не отправлено"
            message.id_client = phone[0]
            message.id_sends = phone[2]
            message.save()
            msg = {
                "id": message.id,
                "phone": phone[1],
                "text": text
            }
            response = ''
            try:
                response = requests.post(f"https://probe.fbrq.cloud/v1/send/{str(message.id)}",
                                         headers={"Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2ODEzNzA3MDYsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Ild5eXNobnlhIn0.1DI1JEM8U0_iy7TNLMTsAFvTda0_tn00OCPJr-HLr_8"},
                                         data=json.dumps(msg))
            except Exception as err:
                error = err
            if response.status_code == 200:
                message.status = "Отправлено"
                message.save()
            else:
                self.schedule.append([phone[1], message, text, error])

    def retry(self):
        for elem in self.schedule:
            error = ''
            elem[1].status = elem[3]
            msg = {
                "id": elem[1].id,
                "phone": elem[0],
                "text": elem[2]
            }
            response = ''
            try:
                response = requests.post(f"https://probe.fbrq.cloud/v1/send/{str(elem[1].id)}",
                                         headers={"Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2ODEzNzA3MDYsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Ild5eXNobnlhIn0.1DI1JEM8U0_iy7TNLMTsAFvTda0_tn00OCPJr-HLr_8"},
                                         data=json.dumps(msg))
            except Exception as err:
                error = err
            if response.status_code == 200:
                elem[1].status = "Отправлено"
                elem[1].save()
                self.schedule.remove(elem)
            else:
                self.schedule.append([elem[0], elem[1], elem[2], error])

    def jobs(self):
        print(self.scheduler.get_jobs())

    def pause_job(self, id):
        self.scheduler.get_job(job_id=id).pause()

    def resume_job(self, id):
        self.scheduler.get_job(job_id=id).resume()

    def remove_job(self, id):
        self.scheduler.remove_job(job_id=id)


@swagger_auto_schema(method='post', request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'date_start': openapi.Schema(type=openapi.TYPE_STRING, description='date "YYYY-MM-DD HH:MM", required'),
        'date_start_new': openapi.Schema(type=openapi.TYPE_STRING, description='date "YYYY-MM-DD HH:MM", not required'),
        'text': openapi.Schema(type=openapi.TYPE_INTEGER, description='text of the message, required'),
        'text_new': openapi.Schema(type=openapi.TYPE_INTEGER, description='text of the message, not required'),
        'tags': openapi.Schema(type=openapi.TYPE_STRING, description='tag or code of operator, required'),
        'tags_new': openapi.Schema(type=openapi.TYPE_STRING, description='tag or code of operator, not required'),
        'date_end': openapi.Schema(type=openapi.TYPE_STRING, description='date "YYYY-MM-DD HH:MM", required'),
        'date_end_new': openapi.Schema(type=openapi.TYPE_STRING, description='date "YYYY-MM-DD HH:MM", not required'),
    }
))
@api_view(['POST'])
@csrf_exempt
def update_send(request):
    if request.method == 'POST':
        body = json.loads(request.body)
        try:
            update_keys = body.keys()
            send = Sends.objects.filter(date_start=pytz.utc.localize(datetime.datetime.strptime(body["date_start"], '%Y-%m-%d %H:%M')),
                                 date_end=pytz.utc.localize(datetime.datetime.strptime(body["date_end"], '%Y-%m-%d %H:%M')),
                                 text=body["text"], tags=body["tags"]).first()
            if 'date_start_new' in update_keys:
                send.date_start = pytz.utc.localize(datetime.datetime.strptime(body["date_start_new"], '%Y-%m-%d %H:%M'))
            if "text_new" in update_keys:
                send.text = body["text_new"]
            if "tags_new" in update_keys:
                send.tags_new = body["tags_new"]
            if "date_end_new" in update_keys:
                send.date_end = pytz.utc.localize(datetime.datetime.strptime(body["date_end_new"], '%Y-%m-%d %H:%M'))
            send.save()
        except Exception as err:
            return HttpResponse(err, status=400)
    return HttpResponse(status=201)


@swagger_auto_schema(method='post', request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'date_start': openapi.Schema(type=openapi.TYPE_STRING, description='date "YYYY-MM-DD HH:MM"'),
        'text': openapi.Schema(type=openapi.TYPE_INTEGER, description='text of the message'),
        'tags': openapi.Schema(type=openapi.TYPE_STRING, description='tag or code of operator'),
        'date_end': openapi.Schema(type=openapi.TYPE_STRING, description='date "YYYY-MM-DD HH:MM"'),
    }
))
@api_view(['POST'])
@csrf_exempt
def delete_send(request):
    if request.method == 'POST':
        body = json.loads(request.body)
        try:
            Sends.objects.filter(date_start=pytz.utc.localize(datetime.datetime.strptime(body["date_start"], '%Y-%m-%d %H:%M')),
                                 date_end=pytz.utc.localize(datetime.datetime.strptime(body["date_end"], '%Y-%m-%d %H:%M')),
                                 text=body["text"], tags=body["tags"]).delete()
        except Exception as err:
            return HttpResponse(err, status=400)
    return HttpResponse(status=201)


class Statistic(APIView):
    def get(self, request):
        sends = Sends.objects.all()
        messages = Message.objects.all()
        result = {}
        for send in sends:
            msgs = []
            for message in messages:
                if message.id_sends == send:
                    msgs.append(message)
            msgs.sort(key=lambda i: i.status)
            msg = [(i.id, i.status) for i in msgs]
            result[send.id] = msg
        return Response(result)


class StatisticDetailed(APIView):
    def get(self, request):
        sends = Sends.objects.all()
        messages = Message.objects.all()
        result = {}
        for send in sends:
            msgs = []
            for message in messages:
                if message.id_sends == send:
                    msgs.append(f'Айди: {message.id} '
                                f'Дата старта: {message.date_st}'
                                f'Статус: {message.status} '
                                f'Айди клиента: {message.id_client} '
                                f'Айди рассылки: {message.id_sends}')
            msgs.sort(key=lambda i: i[2])
            sen = f'Айди: {str(send.id)} ' \
                  f'Дата начала: {str(send.date_start)} ' \
                  f'Текст сообщения: {str(send.text)} ' \
                  f'Теги: {str(send.tags)} ' \
                  f'Дата конца: {str(send.date_end)}'
            result[sen] = msgs
        return Response(result)


scheduler = Scheduler()
