from django.apps import AppConfig


class ClientAddConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'client_add'
