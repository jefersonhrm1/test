from django.urls import path, re_path

from . import views
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
   openapi.Info(
      title="Snippets API",
      default_version='v1',
      description="Test description",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=[permissions.AllowAny],
)

urlpatterns = [
    re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    re_path(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('add_client', views.add_client, name='add_client'),
    path('update_client', views.update_client, name='update_client'),
    path('delete_client', views.delete_client, name='delete_client'),
    path('add_sends', views.add_send, name='add_sends'),
    path('upd_sends', views.update_send, name='upd_sends'),
    path('del_sends', views.delete_send, name='del_sends'),
    path('stat', views.Statistic.as_view(), name='stat'),
    path('statdet', views.StatisticDetailed.as_view(), name='stadet'),
]
