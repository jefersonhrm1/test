## Test
Все файлы в ветке master
## Зависимость от субд
Используется Postgresql

## Начало
Установить через pip все зависимости из файла requirements.txt

Провести все миграции командами ~python3 manage.py makemigrations, ~python3 manage.py migrate, находясь в папке testing
## Запуск
Сервер запускается командой в консоли ~python3 manage.py runserver, находясь в папке testing
## Ссылки
Все ссылки, которые указаны в документации находятся после http://127.0.0.1:8000/client_add/
## OpenAPI
Ссылка на документацию OpenAPI, когда сервер запущен: http://127.0.0.1:8000/client_add/swagger/
## Как отправляются сообщения
Сообщения отправляются каждый день в 12:00

